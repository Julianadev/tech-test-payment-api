using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Models.DTO;

namespace tech_test_payment_api.Services
{
    public interface IValidaQuantidadeProduto
    {
            void ValidaQuantidadeProduto(CadastrarVendaDTO vendaDTO);
    }
}