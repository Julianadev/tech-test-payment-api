using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Models.DTO;

namespace tech_test_payment_api.Services
{
  public interface IVendaServices
  {
    public VendaDTO GetByID(int id);
    public VendaDTO CreateVenda(CadastrarVendaDTO vendaDTO);

    public VendaDTO UpdateVenda(int id, AtualizaVendaDTO vendaDTO);
  }
}