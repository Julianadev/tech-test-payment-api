using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Models.Entity;

namespace tech_test_payment_api.Models.DTO
{
    public class AtualizaVendaDTO
    {
        public StatusVenda StatusVenda{get;set;}
    }
}