using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models.DTO;
using tech_test_payment_api.Models.Entity;
using tech_test_payment_api.Services;
using tech_test_payment_api.Controllers;


namespace PaymentAPI.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class PaymentController : ControllerBase
  {
    private readonly IVendaService _vendaService;

    public PaymentController(IVendaService vendaService)
    {
      _vendaService = vendaService;
    }

    [Route("ObterVendaPorID/{id}")]
    [HttpGet]
    public IActionResult Get(int id)
    {
      var venda = _vendaService.GetByID(id);
      if (venda == null)
      {
        return NotFound();
      }
      return Ok(venda);
    }

    [Route("AtualizarVenda/{id}")]
    [HttpPut]
    public IActionResult Update(int id, AtualizaVendaDTO vendaDTO)
    {

      var venda = _vendaService.UpdateVenda(id, vendaDTO);
      return Ok(venda);
    }

    [HttpPost]
    public IActionResult Criar(Venda venda)
    {
      var vendaCriada = _vendaService.venda(venda);
      return Ok(vendaCriada);
    }

  }
}
