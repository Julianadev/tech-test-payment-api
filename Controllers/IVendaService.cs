using tech_test_payment_api.Models.DTO;
using tech_test_payment_api.Models.Entity;

namespace PaymentAPI.Controllers
{
  public interface IVendaService
  {
    Venda GetByID(int id);
    Venda CreateVenda(CadastrarVendaDTO vendaDTO);
    Venda UpdateVenda(int id, AtualizaVendaDTO vendaDTO);
    object venda(Venda venda);
  }
}